----------------------
Command Line Arguments
----------------------

Usage: java -jar <program jar file> <dataset> <cliqueSize> <parallelism> <executions> <algorithm>

<dataset>
  Specifies the path of the input graph. The input graph is a list of edges (one edge per line), 
  where each edge is represented by a pair of nodes separated by \tab or single-space. 
  Each edge must appear in two directions.
  As example file could be:
  1	2
  1	3
  2	1
  2	3
  3	1
  3	2

<cliqueSize>
  Sets the size of the cliques to be counted. The "cliques" parameter size must be a positive integer 
  greater than or equal to 3.

<parallelism>
  Defines the number of cores explored by the algorithm. The "parallelism" parameter must be a
  positive integer less than or equal to the maximum number of cores in the used platform.
  The parallelism is irrelevant to L+N algorithm, however a number must be set in order to
  follow the sequence of input commands.

<executions>
  Specifies the number of times the algorithm will repeat the same execution. Hence, the output
  runtime is the average of the running times of each execution. The "executions" parameter 
  must be a positive integer greater than or equal to 1. 

<algorithm>
  Defines which algorithm will run. The "algorithm" parameter must be an integer from 0 to 4.
  Each algorithm is represented by an integer as following:
  0 = L+N
  1 = 1by1
  2 = 2by2
  3 = 1-2by2
  4 = 3by3

Output:
  The number of cliques and the running time spent by the algorithm to count the cliques. Moreover,
  the time spent by the garbage collector is presented (which is included in the running time) in
  order to control it.

Example: let graph.txt be the input graph stored in the folder "/home/user/" and let 
countcliques.jar be the program jar file. An exemple of command to execute the program could be:

	java -jar countcliques.jar /home/user/graph.txt 5 8 1 1

The command above would run once the 1by1 algorithm to count the number of cliques of size 5 on 
graph.txt using 8 cores. If this graph contains 100 cliques of size 5, an example of output could be:
  Clique count: 100
  Running time using 8 cores of 1by1 is 42 ms - GC Time: 2 ms

