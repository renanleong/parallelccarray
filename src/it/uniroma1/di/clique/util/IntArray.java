package it.uniroma1.di.clique.util;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class IntArray {

	public int[] data = null;
	private int used = 0;

	public IntArray() {
		data = new int[10]; // same as in ArrayList
	}

	public IntArray(boolean empty) {
		data = null;
	}

	public void addIfEmpty(int value) {

		if (data == null)
			data = new int[10];
		else if (used >= data.length)
			grow(used + 1);

		data[used] = value;
		used += 1;
	}

	public void add(int value) {

		if (used >= data.length)
			grow(used + 1);

		data[used] = value;
		used += 1;
	}

	public int get(int index) {
		return data[index];
	}

	private void grow(int minCapacity) {

		// taken from ArrayList

		// overflow-conscious code
		int oldCapacity = data.length;
		int newCapacity = oldCapacity + (oldCapacity >> 1);
		if (newCapacity - minCapacity < 0)
			newCapacity = minCapacity;

		// minCapacity is usually close to size, so this is a win:
		data = Arrays.copyOf(data, newCapacity);
	}

	public int size() {
		return used;
	}

	public boolean contains(int value) {
		for (int i = 0; i < used; i++)
			if (data[i] == value)
				return true;

		return false;
	}

	public void sort(Comparator c) {
		Collections.sort(new FakeList(data, used), c);
	}

	class FakeList extends AbstractList<Integer>{

		int[] data;
		int size;
		
		public FakeList(int[] data, int size){
			this.data = data;
			this.size = size;
		}
		
		@Override
		public Integer get(int index) {
			return data[index];
		}

		@Override
		public int size() {
			return size;
		}
		
		@Override
		public Integer set(int index, Integer element){
			int v = data[index];
			data[index] = element;
			return v;
		}
		
	}

	public void sort() {
		Arrays.sort(data, 0, used);
	}

	public IntArrayIterator iterator() {
		return new IntArrayIterator(this);
	}

	public static class IntArrayIterator {

		public IntArray data;
		public int index = 0;

		public IntArrayIterator(IntArray data) {
			this.data = data;
		}

		public boolean hasNext() {
			return index < data.size();
		}

		public int next() {
			return data.get(index++);
		}

	}

	public static IntArray intersection(IntArray A, IntArray B) {

		
		IntArray common = new IntArray(true);
		int indexA = 0;
		int indexB = 0;
		
		while (indexA < A.size() && indexB < B.size()) {

			int a = A.get(indexA);
			int b = B.get(indexB);

			if (a < b)
				indexA += 1;
			else if (a > b)
				indexB += 1;
			else {
				common.addIfEmpty(a);
				indexA += 1;
				indexB += 1;
			}
		}

		return common.size() == 0 ? null : common;
	}
	
	public static IntArray intersection(IntArray A, IntArray B, int[] degrees) {

		
		IntArray common = new IntArray(true);
		int indexA = 0;
		int indexB = 0;
		
		while (indexA < A.size() && indexB < B.size()) {

			int a = A.get(indexA);
			int b = B.get(indexB);

			if (degrees[a] < degrees[b])
				indexA += 1;
			else if (degrees[b] < degrees[a])
				indexB += 1;
			else {
				if(a < b)
					indexA += 1;
				else if (b < a)
					indexB += 1;
				else{
					common.addIfEmpty(a);
					indexA += 1;
					indexB += 1;
				}
			}
		}

		return common.size() == 0 ? null : common;
	}

	public static IntArray intersection(IntArray A, IntArray B, IntArray C, int u, int v) {

		/*
		System.out.println("A: " + A);
		System.out.println("B: " + B);
		System.out.println("C: " + C);
		System.out.println("u: " + u);
		System.out.println("v: " + v);
		*/

		IntArray common = new IntArray(true);
		int indexA = 0;
		int indexB = 0;
		int indexC = 0;

		boolean found = false;

		while (indexA < A.size() && indexB < B.size() && indexC < C.size()) {

			int a = A.get(indexA);
			int b = B.get(indexB);
			int c = C.get(indexC);

			if (!found) {
				if (b == v)
					found = true;
				else if (c == u)
					found = true;
			}

			if (a < b)
				indexA += 1;
			else if (a > b)
				indexB += 1;
			// other cases have a == b
			else if (a < c) { // since b == a then b < c
				indexA += 1;
				indexB += 1;
			} else if (c < a) {
				indexC += 1;
			} else {
				common.addIfEmpty(a);
				indexA += 1;
				indexB += 1;
				indexC += 1;
			}
		}

		if (found)
			return common.size() == 0 ? null : common;

		else if (common.size() > 0) {

			boolean stopB = indexB >= B.size();
			boolean stopC = indexC >= C.size();
			while (!stopB || !stopC) {

				if (!stopB) {
					int b = B.get(indexB);
					if (b == v)
						return common;

					indexB += 1;
					stopB = indexB >= B.size();
				}

				if (!stopC) {
					int c = C.get(indexC);
					if (c == u)
						return common;

					indexC += 1;
					stopC = indexC >= C.size();
				}
			}

			return null;
		}


		/*
		if (common.size() > 0) {

			if(Arrays.binarySearch(B.data, indexB, B.size(), v) >= 0)
				return common;

			if(Arrays.binarySearch(C.data, indexC, C.size(), u) >= 0)
				return common;
		}
		*/

		return null;
	}

	public String toString() {
		String s = "[";
		for (int i = 0; i < used; i++) {
			s += data[i] + ", ";
		}
		return s + "]";
	}

	public void set(int index, int value) {
		data[index] = value;
	}

	public void setSize(int newSize) {
		this.used = newSize;
	}

	public static void main(String[] args) {

		IntArray A = new IntArray();
		IntArray B = new IntArray();
		IntArray C = new IntArray();

		A.add(9);
		A.add(10);
		A.add(12);

		B.add(9);

		C.add(9);
		C.add(10);

		System.out.println(A.intersection(A, B, C, 10, 12));
	}
}
