package it.uniroma1.di.clique;

import it.uniroma1.di.clique.util.IntArray;
import it.uniroma1.di.clique.util.IntToIntArray;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;


public class ParEnumTequalK {

    protected IntToIntArray nodes = new IntToIntArray();
    public static ParEnumTequalK instance;
    public int cliqueSize;
    public int max = 10000;
    public int expansionSize;
    
    private ParEnumTequalK() {}

    public ParEnumTequalK(String pathFile) throws IOException {

        instance = this;

        // parse the input file and add edges to the graph
        try {

            BufferedReader br = new BufferedReader(new FileReader(pathFile));
            String line;
            while ((line = br.readLine()) != null) {

                String[] l = line.split(" ");
                if (l.length == 1)
                    l = line.split("\t");

                this.addEdge(l[0], l[1]);
            }
            br.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public long countCliquesOfSize(int cliqueSize, int parallelism) {

    	// compute the total order ≺ over the nodes and sort neighbors lists
        prepareForCounting();

        this.cliqueSize = cliqueSize;
        this.expansionSize = cliqueSize-2;
                		
        // create pool of threads
        ForkJoinPool fjPool = new ForkJoinPool(parallelism);

        // start computation
        return fjPool.invoke(new GraphCompute(-1, null, null, -1, -1));
    }

    public void addEdge(String a, String b) {

        int v = Integer.parseInt(a);
        int y = Integer.parseInt(b);

        if (nodes.containsKey(v)) {
            nodes.get(v).add(y);
        } else {
            IntArray arr = new IntArray();
            arr.add(y);
            nodes.put(v, arr);
        }
    }

    public void addEdge(int v, int y) {

        if (nodes.containsKey(v)) {
            nodes.get(v).add(y);
        } else {
            IntArray arr = new IntArray();
            arr.add(y);
            nodes.put(v, arr);
        }
        if (!nodes.containsKey(y)){
        	IntArray arr = new IntArray();
        	nodes.put(y, arr);
        }
    }

    public int getNodesNumber() {
        return nodes.size();
    }

    public IntArray getNeighbors(int node) {
        return nodes.get(node);
    }
    
    public int[] findSubset(long position, int size){
    	
    	int[] subset = new int[expansionSize];
    	int element = 1;
    	int index = 1;
    	long lastPosition = 1;
    	
    	//System.out.println("Position:" + position + " size:" + size);
    	
    	if(expansionSize == 1)
    		subset[0] =  (int) position-1;
    	else{
    		do{
    			lastPosition = 1;
    			for (int i = 1, m = (size - element); i <= (expansionSize - index); i++, m--)
    				lastPosition = lastPosition * m / i;
    		
    			if (position <= lastPosition){
    				subset[index-1] = element-1;
    				index++;
    			}
    			else
    				position = position - lastPosition;
    			element++;
    		}while(index < expansionSize);
    	
    		//System.out.println("SubsetSize:" + subset.length + " index-1:" + (index-1));
    	
    		subset[index-1] = element + ((int) position) - 2;
    	}
    	return subset;
    }
    
    public boolean checkAdj(int[] subset, IntArray candidates){
    	
    	boolean adj = true;
    	
    	for (int j = 0; j < subset.length-1; j++) {
			for (int j2 = j+1; j2 < subset.length; j2++) {
				if (nodes.get(candidates.get(subset[j])) == null && 
						nodes.get(candidates.get(subset[j2])) == null){
					adj = false;
					break;
				}
				else if(nodes.get(candidates.get(subset[j])) != null &&
						nodes.get(candidates.get(subset[j2])) == null &&
						!nodes.get(candidates.get(subset[j])).contains(candidates.get(subset[j2]))){
					adj = false;
					break;
				}
				else if(nodes.get(candidates.get(subset[j])) == null &&
						nodes.get(candidates.get(subset[j2])) != null &&
						!nodes.get(candidates.get(subset[j2])).contains(candidates.get(subset[j]))){
					adj = false;
					break;
				}
				else if(nodes.get(candidates.get(subset[j])) != null &&
						nodes.get(candidates.get(subset[j2])) != null &&
						!nodes.get(candidates.get(subset[j])).contains(candidates.get(subset[j2])) &&
						!nodes.get(candidates.get(subset[j2])).contains(candidates.get(subset[j]))){
					adj = false;
					break;
				}
			}
			if (!adj)
			break;
		}
    	
    	return adj;
    	
    }
    
    class GraphCompute extends RecursiveTask<Long> {

        private int vertex;
        public GraphCompute parent = null;
        public GraphCompute next = null;
        public IntArray candidates = null;
        private long lo;
        private long hi;

        public GraphCompute(int vertex, GraphCompute parent, IntArray candidates, long lo, long hi) {
            this.vertex = vertex;
            this.parent = parent;
            this.candidates = candidates;
            this.lo = lo;
            this.hi = hi;
        }

        @Override
        protected Long compute() {

            ParEnumTequalK graph = ParEnumTequalK.instance;
            int cliqueSize = graph.cliqueSize;
            GraphCompute prev = null;

            long count = 0;
            
            // Check if it is the first computation
            if (lo < 0){
				
				for(int v = 0; v < graph.getNodesNumber(); v++){
					IntArray list = graph.getNeighbors(v);
					
					if (list != null && list.size() >= cliqueSize - 1) {
						
						long subsets = 1;
						for (int j = 1, m = list.size(); j <= expansionSize; j++, m--)
							subsets = subsets * m / j;
						
						if(subsets <= max){
							//System.out.println("v:" + v + " subsets:" + subsets);
							GraphCompute gc = new GraphCompute(v, this, list, 1, subsets);
			            	gc.next = prev;
							gc.fork();
							prev = gc;
						}
						else{
							long groups = (subsets/max) + ((subsets % max == 0) ? 0 : 1);
							//System.out.println("Groups:" + groups);
							for (int i = 1; i <= groups; i++) {
								long first = (i-1) * max + 1;
								GraphCompute gc = new GraphCompute(v, this, list, first, i*max);
				            	gc.next = prev;
								gc.fork();
								prev = gc;
							}
						}
					}
				}
            }
            else{
            	
            	boolean hasNeighbors, adj;
            	
            	/*System.out.println();
            	System.out.println("vertex:" + vertex + " lo:" + lo +  " expansionSize:" + expansionSize);
            	System.out.print("Candidates:");
            	for (int i = 0; i < candidates.size(); i++) {
					System.out.print(" " + candidates.get(i));
				}
            	System.out.println();*/
            	
            	int[] indicesSubset = new int[expansionSize];
            	
            	if(lo > 1)
            		indicesSubset = findSubset(lo, candidates.size());
            	else{
            		for (int i = 0; i < indicesSubset.length; i++) 
            			indicesSubset[i] = i;
            	}
            	            	
            	/*System.out.print("Subset:");
            	for (int i = 0; i < indicesSubset.length; i++) {
					System.out.print(" " + candidates.get(indicesSubset[i]));
				}
            	System.out.println();*/
            	
            	for (long i = lo; i <= hi; i++) {
            		
            		hasNeighbors = true;            	
            		adj = checkAdj(indicesSubset, candidates);
            		     
            		if(adj){
            			
            			int subset[] = new int[expansionSize];
        			
           				IntArray nextCandidates = candidates;
        			
            			for (int j = 0; j < subset.length; j++) {
            			
            				subset[j] = candidates.get(indicesSubset[j]);
            				//System.out.println("Subset: " + subset[0]);
            			
            				if (nextCandidates != null && nextCandidates.size() >= 1 
            						&& nodes.get(subset[j]) != null && 
            						nodes.get(subset[j]).size() >= 1){
            					nextCandidates = IntArray.intersection(nextCandidates, nodes.get(subset[j]));
            				}
            				else{
            					//System.out.println("In!");
            					hasNeighbors = false;
            					break;
            				}
            			}
            				
            			if(hasNeighbors && nextCandidates != null){
            				//System.out.println("Count!");
            				count += nextCandidates.size();
            			}
            		}
            		
            		if ((indicesSubset[0]) == candidates.size() - expansionSize)
            			break;
            		
            		//expand
            		if (indicesSubset[expansionSize-1] < candidates.size()-1)
            			indicesSubset[expansionSize-1]++;
            		//reduce
            		else{
            			int j = 1;
            			while (indicesSubset[expansionSize - j - 1] == candidates.size() - j - 1)
            				j++;
            			indicesSubset[expansionSize - j - 1]++;
            			while (j > 0){
            				j--;
            				indicesSubset[expansionSize - j - 1] = indicesSubset[expansionSize - j - 2] + 1;
            			}
            		}
            	}
            	
            }
            //retrieve results from pending computations
        	while (prev != null) {
        		count += prev.join();
        		prev = prev.next;
        	}
            return count;
        }
    }

    private void prepareForCounting() {

        ParEnumTequalK gi = new ParEnumTequalK();
        
        for (int u = 0; u < this.nodes.arrNodes.length; u++){
            IntArray uList = this.getNeighbors(u);
            
            if(uList == null)
				continue;
            
            for (int i = 0; i < uList.size(); i++) {

                int v = uList.get(i);
                IntArray vList = this.getNeighbors(v);

                if (uList.size() < vList.size() || (uList.size() == vList.size() && u < v)){
                    gi.addEdge(u, v);
                }
            }
        }

        this.nodes = gi.nodes;

        // sort neighbors list
        for (int u = 0; u < this.nodes.arrNodes.length; u++){
			
			if(this.nodes.get(u) == null)
				continue;
         
			this.nodes.get(u).sort();
        }
        
       /* for (int i = 0; i < nodes.size(); i++) {
			System.out.print(i + ":");
			if(nodes.get(i) != null){
				for (int j = 0; j < nodes.get(i).size(); j++) {
					System.out.print(" " + nodes.get(i).get(j));
				}
			}
			System.out.println();
		}*/
    }
}
