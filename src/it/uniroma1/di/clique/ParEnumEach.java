package it.uniroma1.di.clique;

import it.uniroma1.di.clique.util.IntArray;
import it.uniroma1.di.clique.util.IntToIntArray;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

public class ParEnumEach {

    protected IntToIntArray nodes = new IntToIntArray();
    public static ParEnumEach instance;
    public int cliqueSize;
    
    private ParEnumEach() {}

    public ParEnumEach(String pathFile) throws IOException {

        instance = this;

        // parse the input file and add edges to the graph
        try {

            BufferedReader br = new BufferedReader(new FileReader(pathFile));
            String line;
            while ((line = br.readLine()) != null) {

                String[] l = line.split(" ");
                if (l.length == 1)
                    l = line.split("\t");

                this.addEdge(l[0], l[1]);
            }
            br.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public long countCliquesOfSize(int cliqueSize, int parallelism, int expansionSize) {

    	// compute the total order ≺ over the nodes and sort neighbors lists
        prepareForCounting();

        this.cliqueSize = cliqueSize;
        
        IntArray candidates = new IntArray();
		for (int i = 0; i < nodes.size(); i++) {
			candidates.add(i);
		}

        // create pool of threads
        ForkJoinPool fjPool = new ForkJoinPool(parallelism);

        // start computation
        return fjPool.invoke(new GraphCompute(null, null, candidates, 0, expansionSize, 0));
    }

    public void addEdge(String a, String b) {

        int v = Integer.parseInt(a);
        int y = Integer.parseInt(b);

        if (nodes.containsKey(v)) {
            nodes.get(v).add(y);
        } else {
            IntArray arr = new IntArray();
            arr.add(y);
            nodes.put(v, arr);
        }
    }

    public void addEdge(int v, int y) {

        if (nodes.containsKey(v)) {
            nodes.get(v).add(y);
        } else {
            IntArray arr = new IntArray();
            arr.add(y);
            nodes.put(v, arr);
        }
        if (!nodes.containsKey(y)){
        	IntArray arr = new IntArray();
        	nodes.put(y, arr);
        }
    }

    public int getNodesNumber() {
        return nodes.size();
    }

    public IntArray getNeighbors(int node) {
        return nodes.get(node);
    }
    
    public int[] findSubset(int position, int size, int expansionSize){
    	
    	int[] subset = new int[expansionSize];
    	int element = 1;
    	int index = 1;
    	double lastPosition = 1;
    	
    	do{
    		lastPosition = 1;
    		
    		for (int i = 1, m = (size - element); i <= (expansionSize - index); i++, m--)
    			lastPosition = lastPosition * m / i;
    		if (position <= lastPosition){
    			subset[index-1] = element-1;
    			index++;
    		}
    		else
    			position = position - (int) lastPosition;
    		
    		element++;
    	}while(index < expansionSize);
    	
    	subset[index-1] = element + position - 2;
    	
    	//return index +1
    	return subset;
    }
    
    public boolean checkAdj(int[] subset, IntArray candidates){
    	
    	boolean adj = true;
    	
    	for (int j = 0; j < subset.length-1; j++) {
			for (int j2 = j+1; j2 < subset.length; j2++) {
				//System.out.println("Check " + j + ":" + candidates.get(subset[j]) +  ":" + 
						//nodes.get(candidates.get(subset[j])) + " and " + j2 + ":" + candidates.get(subset[j2]) 
						//+  ":" + nodes.get(candidates.get(subset[j2])));
				if (nodes.get(candidates.get(subset[j])) == null && 
						nodes.get(candidates.get(subset[j2])) == null){
					adj = false;
					break;
				}
				else if(nodes.get(candidates.get(subset[j])) != null &&
						nodes.get(candidates.get(subset[j2])) == null &&
						!nodes.get(candidates.get(subset[j])).contains(candidates.get(subset[j2]))){
					adj = false;
					break;
				}
				else if(nodes.get(candidates.get(subset[j])) == null &&
						nodes.get(candidates.get(subset[j2])) != null &&
						!nodes.get(candidates.get(subset[j2])).contains(candidates.get(subset[j]))){
					adj = false;
					break;
				}
				else if(nodes.get(candidates.get(subset[j])) != null &&
						nodes.get(candidates.get(subset[j2])) != null &&
						!nodes.get(candidates.get(subset[j])).contains(candidates.get(subset[j2])) &&
						!nodes.get(candidates.get(subset[j2])).contains(candidates.get(subset[j]))){
					adj = false;
					break;
				}
			}
			if (!adj)
			break;
		}
    	
    	return adj;
    	
    }
    
    class GraphCompute extends RecursiveTask<Long> {

        private int[] vertices;
        public GraphCompute parent = null;
        public GraphCompute next = null;
        public IntArray candidates = null;
        double position;
        int expansionSize;
        private int depth;

        public GraphCompute(int[] vertices, GraphCompute parent, IntArray candidates, double position, 
        		int expansionSize, int depth) {
            this.vertices = vertices;
            this.parent = parent;
            this.candidates = candidates;
            this.position = position;
            this.expansionSize = expansionSize;
            this.depth = depth;
        }

        @Override
        protected Long compute() {
        	
        	//Scanner scan = new Scanner(System.in);

            ParEnumEach graph = ParEnumEach.instance;
            int cliqueSize = graph.cliqueSize;
            GraphCompute prev = null;

            long count = 0;
            
            int counter = 0;
                        
            /*System.out.println();
            System.out.println("Começou depth:" + depth);
            
            System.out.println("Position:" + position);*/
            //scan.next();
            
            // Check if it is the first computation
            if (position < 1) {
            	
            	//System.out.println("Entrou 1");
            	
            	double subsets = 1;
            	for (int i = 1, m = candidates.size(); i <= expansionSize; i++, m--)
					subsets = subsets * m / i;
            	
            	//System.out.println("First:" + first + " last:" + last + " med:" + med);
            	for (int i = 1; i <= subsets; i++) {
            		GraphCompute gc = new GraphCompute(null, null, candidates, i, expansionSize, depth);
                    gc.next = prev;
                    counter++;
                    gc.fork();
                    prev = gc;
                    
                    if(counter > 1000000){
                		while (prev != null) {
                    		count += prev.join();
                        	prev = prev.next;
                		}
                		prev = null;
                		counter = 0;
                	}
				}
            	
                while (prev != null) {
            		count += prev.join();
                	prev = prev.next;
            	}
            }
            else{

            	//System.out.println("Entrou 2");
            	
            	boolean hasNeighbors, adj;
            	
            	int[] subset = findSubset((int) position, candidates.size(), expansionSize);
            	
            	/*System.out.print("Subset = ");
            	for (int j = 0; j < expansionSize; j++) {
					System.out.print(candidates.get((subset[j])) + " ");
				}
            	System.out.println();*/
            		
            	hasNeighbors = true;            	
            	adj = checkAdj(subset, candidates);
            		
            	if((depth + expansionSize == cliqueSize) && adj){
            		count++;
            		//System.out.println("3- Count 1");
            		adj = false;
            	}
            	            		
            	int u = candidates.get(subset[0]);
            		
            	/*System.out.println("u="+u);
            	if(nodes.get(u) != null){
            	for (int k = 0; k < nodes.get(u).size(); k++) {
					System.out.print(nodes.get(u).get(k) + " ");
				}}*/
            		            		
            	if(nodes.get(u) != null && 
        			nodes.get(u).size() >= cliqueSize - (depth + expansionSize) && adj){
        		
            		IntArray nextCandidates = IntArray.intersection(candidates, nodes.get(u));
        		
            		for (int i = 1; i < expansionSize; i++) {
            			
            			int v = candidates.get(subset[i]);
            			if (nextCandidates != null && 
            					nextCandidates.size() >= cliqueSize - (depth + expansionSize) && 
            					nodes.get(v) != null && 
            					nodes.get(v).size() >= cliqueSize - (depth + expansionSize)){
            				
            				/*System.out.println("v=" + v);
            				for (int k = 0; k < nodes.get(v).size(); k++) {
								System.out.print(nodes.get(v).get(k) + " ");
							}
            				System.out.println();
            				System.out.println("Next");
            				for (int k = 0; k < nextCandidates.size(); k++) {
								System.out.print(nextCandidates.get(k) + " ");
							}
            				System.out.println();*/
            				
            				nextCandidates = IntArray.intersection(nextCandidates, nodes.get(v));
            				
            				/*if(nextCandidates != null){
            				System.out.println("Next2");
            				for (int k = 0; k < nextCandidates.size(); k++) {
								System.out.print(nextCandidates.get(k) + " ");
							}
            				System.out.println();}*/
            			}
            			else{
            				hasNeighbors = false;
            				break;
            			}
        			
            			/*System.out.println("hasNeigh=" + hasNeighbors + "nextCa=" + (nextCandidates!=null));
            			if(nextCandidates != null)
            				System.out.println("nextSize=" + nextCandidates.size() + " count " + cliqueSize + "-(" + depth + "+" + expansionSize + ")");
            				*/	
            			if(hasNeighbors && nextCandidates != null && 
            					nextCandidates.size() >= cliqueSize - (depth + expansionSize)){

            				if(depth == cliqueSize - expansionSize - 1){
            					//System.out.println("3: Count " + nextCandidates.size());
            					count += nextCandidates.size();
            				}
            				else{
            					int newExpansion;
            					if(depth + expansionSize > cliqueSize - expansionSize)
            						newExpansion = cliqueSize - depth - expansionSize;
            					else
            						newExpansion = expansionSize;
            					
            					double subsets = 1;
            	            	for (int j = 1, m = nextCandidates.size(); j <= expansionSize; j++, m--)
            						subsets = subsets * m / j;
            	            	
            	            	//System.out.println("First:" + first + " last:" + last + " med:" + med);
            	            	for (int j = 1; j <= subsets; j++) {
            	            		GraphCompute gc = new GraphCompute(null, null, nextCandidates, j, newExpansion, 
            	            				(depth+expansionSize));
            	                    gc.next = prev;
            	                    gc.fork();
            	                    prev = gc;
            					}
            				}
            			}
            		}
            	}
            	//retrieve results from pending computations
            	while (prev != null) {
            		count += prev.join();
            		prev = prev.next;
            	}
            }
            return count;
        }

    }

    private void prepareForCounting() {

        ParEnumEach gi = new ParEnumEach();
        
        for (int u = 0; u < this.nodes.arrNodes.length; u++){
            IntArray uList = this.getNeighbors(u);
            
            if(uList == null)
				continue;
            
            for (int i = 0; i < uList.size(); i++) {

                int v = uList.get(i);
                IntArray vList = this.getNeighbors(v);

                if (uList.size() < vList.size() || (uList.size() == vList.size() && u < v)){
                    gi.addEdge(u, v);
                }
            }
        }

        this.nodes = gi.nodes;

        // sort neighbors list
        for (int u = 0; u < this.nodes.arrNodes.length; u++){
			
			if(this.nodes.get(u) == null)
				continue;
         
			this.nodes.get(u).sort();
        }
        
       /* for (int i = 0; i < nodes.size(); i++) {
			System.out.print(i + ": ");
			
			if(nodes.get(i) == null){
				System.out.println();
				continue;
			}
				
			
			for (int j = 0; j < nodes.get(i).size(); j++) {
				System.out.print(nodes.get(i).get(j) + " ");
			}
			System.out.println();
		}*/

    }
}
