package it.uniroma1.di.clique;

import java.io.IOException;
import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;

public class Main {

	public static void main(String[] args) throws IOException {
		
		if(args.length < 5){
			System.out.println("<dataset> <cliqueSize> <parallelism> <executions> <algorithm>");
			System.exit(0);
		}
	
		int repetitions = Integer.parseInt(args[3]);
		int alg = Integer.parseInt(args[4]);
		int cliqueSize = Integer.parseInt(args[1]);
		long sum = 0;
		
		// L+N
		if (alg == 0) { 
			sum = 0;
			long count = 0;
			for (int i = 0; i < repetitions; i++) {
				if (repetitions > 1)
					System.out.println("Iteration: " + (i + 1));
				long start = System.currentTimeMillis();
				LPN lpn = LPN.build(args);
				count = lpn.countCliquesOfSize(cliqueSize, 1);
				sum += System.currentTimeMillis() - start;
				if (i == repetitions - 1)
					System.out.println("Clique count: " + count);
					//System.out.println("Neighborhoods: " + lpn.counterNeighborhoods);
			}
			System.out.println(
					"Running time using of L+N code is " + 
					(sum/repetitions) + " ms - GC Time: " + (getGarbageCollectionTime()/repetitions) + " ms");
		
		}
		
		
		// 1by1
		if (alg == 1) {
			sum = 0;
			long count = 0;
			for (int i = 0; i < repetitions; i++) {
				if (repetitions > 1)
					System.out.println("Iteration: " + (i + 1));
				long start = System.currentTimeMillis();
				OneByOne graph = new OneByOne(args[0]);
				count = graph.countCliquesOfSize(cliqueSize, Integer.parseInt(args[2]));
				sum += System.currentTimeMillis() - start;
				if (i == repetitions - 1) {
					System.out.println("Clique count: " + count);
					//System.out.println("Neighborhoods: " + graph.counterNeighborhoods);
				}
			}
			System.out.println(
					"Running time using " + args[2] + " cores of 1by1 code is " + 
					(sum/repetitions) + " ms - GC Time: " + (getGarbageCollectionTime()/repetitions) + " ms");
			//System.out.println();
		}
		
		// 2by2
		if (alg == 2) {
			sum = 0;
			long count = 0;
			for (int i = 0; i < repetitions; i++) {
				if (repetitions > 1)
					System.out.println("Iteration: " + (i + 1));
				long start = System.currentTimeMillis();
				TwoByTwo graph = new TwoByTwo(args[0]);
				count = graph.countCliquesOfSize(cliqueSize, Integer.parseInt(args[2]));
				sum += System.currentTimeMillis() - start;
				if (i == repetitions - 1) {
					System.out.println("Tasks: " + graph.counterTasks);
					System.out.println("Subsets: " + graph.counterSubsets);
					System.out.println("Clique count: " + count);
				}
			}
			System.out.println(
					"Running time using " + args[2] + " cores of 2by2 is " + 
					(sum/repetitions) + " ms - GC Time: " + (getGarbageCollectionTime()/repetitions) + " ms");
		}
		
		//1-2by2
		if (alg == 3) { 
			sum = 0;
			long count = 0;
			for (int i = 0; i < repetitions; i++) {
				if (repetitions > 1)
					System.out.println("Iteration: " + (i + 1));
				long start = System.currentTimeMillis();
				OneTwoByTwo graph = new OneTwoByTwo(args[0]);
				count = graph.countCliquesOfSize(cliqueSize, Integer.parseInt(args[2]));
				sum += System.currentTimeMillis() - start;
				if (i == repetitions - 1) {
					System.out.println("Clique count: " + count);
				}
			}
			System.out.println(
					"Running time using " + args[2] + " cores of 1-2by2 is " + 
					(sum/repetitions) + " ms - GC Time: " + (getGarbageCollectionTime()/repetitions) + " ms");
		}
		
		//3by3
		if (alg == 4) { 
			sum = 0;
			long count = 0;
			for (int i = 0; i < repetitions; i++) {
				if (repetitions > 1)
					System.out.println("Iteration: " + (i + 1));
				long start = System.currentTimeMillis();
				ThreeByThree graph = new ThreeByThree(args[0]);
				count = graph.countCliquesOfSize(cliqueSize, Integer.parseInt(args[2]));
				sum += System.currentTimeMillis() - start;
				if (i == repetitions - 1) {
					System.out.println("Tasks: " + graph.counterTasks);
					System.out.println("Subsets: " + graph.counterSubsets);
					System.out.println("Clique count: " + count);
				}
			}
			System.out.println(
					"Running time using " + args[2] + " cores of 3by3 is " + 
					(sum/repetitions) + " ms - GC Time: " + (getGarbageCollectionTime()/repetitions) + " ms");
		}
		
		
		//ParEnumGroupMax
		if (alg == 5) { 
			if(args.length < 6){
				System.out.println("<dataset> <cliqueSize> <parallelism> <executions> <algorithm> <expansionSize>");
				System.exit(0);
			}
			
			sum = 0;
			long count = 0;
			
			int expansionSize = Integer.parseInt(args[5]);
			
			for (int i = 0; i < repetitions; i++) {
				if (repetitions > 1)
					System.out.println("Iteration: " + (i + 1));
				long start = System.currentTimeMillis();
				ParEnumGroupMax graph = new ParEnumGroupMax(args[0]);
				count = graph.countCliquesOfSize(cliqueSize, Integer.parseInt(args[2]), expansionSize);
				sum += System.currentTimeMillis() - start;
				if (i == repetitions - 1) {
					System.out.println("Tasks: " + graph.counterTasks);
					System.out.println("Subsets: " + graph.counterSubsets);
					System.out.println("Nodes fixed: " + graph.counterNodesFixed);
					System.out.println("Clique count: " + count);
				}
			}
			System.out.println(
					"Running time using " + args[2] + " cores and " + expansionSize + "-expansion and " 
					+	"ParEnumGroupMax is " + (sum/repetitions) + " ms - GC Time: " 
					+ (getGarbageCollectionTime()/repetitions) + " ms");
		}
		//ParEnumTequalK
		if (alg == 6) {
					
			sum = 0;
			long count = 0;
					
			for (int i = 0; i < repetitions; i++) {
				if (repetitions > 1)
					System.out.println("Iteration: " + (i + 1));
				long start = System.currentTimeMillis();
				ParEnumTequalK graph = new ParEnumTequalK(args[0]);
				count = graph.countCliquesOfSize(cliqueSize, Integer.parseInt(args[2]));
				sum += System.currentTimeMillis() - start;
				if (i == repetitions - 1) {
					System.out.println("Clique count: " + count);
				}
			}
			System.out.println(
					"Running time using " + args[2] + " cores of ParEnumTequalK is " + (sum/repetitions) + 
					" ms - GC Time: " + (getGarbageCollectionTime()/repetitions) + " ms");
		}
		
	}
	
	private static long getGarbageCollectionTime() {
		long collectionTime = 0;
		for (GarbageCollectorMXBean garbageCollectorMXBean : ManagementFactory
				.getGarbageCollectorMXBeans()) {
			collectionTime += garbageCollectorMXBean.getCollectionTime();
		}
		return collectionTime;
	}
}