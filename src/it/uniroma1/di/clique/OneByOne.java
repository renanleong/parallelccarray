package it.uniroma1.di.clique;

import it.uniroma1.di.clique.util.IntArray;
import it.uniroma1.di.clique.util.IntToIntArray;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

public class OneByOne implements CliqueCounterGraph {

	protected IntToIntArray nodes = new IntToIntArray();
	public static OneByOne instance;
	public int cliqueSize;
	//public static volatile long counterNeighborhoods = 0;

	private OneByOne() {}
	
	public OneByOne(String pathFile) throws IOException {

		instance = this;
		//counterNeighborhoods = 0;

		// parse the input file and add edges to the graph
		try {

			BufferedReader br = new BufferedReader(new FileReader(pathFile));
			String line;
			while ((line = br.readLine()) != null) {

				String[] l = line.split(" ");
				if (l.length == 1)
					l = line.split("\t");

				this.addEdge(l[0], l[1]);
			}
			br.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public long countCliquesOfSize(int cliqueSize, int parallelism) {

		// compute the total order ≺ over the nodes and sort neighbors lists
		prepareForCounting();

		this.cliqueSize = cliqueSize;

		// create pool of threads
		ForkJoinPool fjPool = new ForkJoinPool(parallelism);

		// start computation
		return fjPool.invoke(new GraphCompute(-1, null, null, 0));
	}

	@Override 
	public void addEdge(String a, String b) {

		int v = Integer.parseInt(a);
		int y = Integer.parseInt(b);

		if (nodes.containsKey(v)) {
			nodes.get(v).add(y);
		} else {
			IntArray arr = new IntArray();
			arr.add(y);
			nodes.put(v, arr);
		}
	}
	
	public void addEdge(int v, int y) {
		
		if (nodes.containsKey(v)) {
			nodes.get(v).add(y);
		} else {
			IntArray arr = new IntArray();
			arr.add(y);
			nodes.put(v, arr);
		}
	}

	@Override 
	public int getNodesNumber() {
		return nodes.size();
	}

	public IntArray getNeighbors(int node) {
		return nodes.get(node);
	}
	
//	public static synchronized void increaseCounter() {
//		counterNeighborhoods += 1;
//	}

	class GraphCompute extends RecursiveTask<Long> {

		private int vertex;
		public GraphCompute parent = null;
		public GraphCompute next = null;
		public IntArray candidates = null;
		private int depth;

		public GraphCompute(int vertex, GraphCompute parent, IntArray candidates, int depth) {
			this.vertex = vertex;
			this.parent = parent;
			this.candidates = candidates;
			this.depth = depth;
		}

		@Override
		protected Long compute() {

			OneByOne graph = OneByOne.instance;
			int cliqueSize = graph.cliqueSize;
			GraphCompute prev = null;

			long count = 0;
			if (vertex < 0) {
				for(int v = 0; v < graph.getNodesNumber(); v++){
					IntArray list = graph.getNeighbors(v);
					if (list != null && list.size() >= cliqueSize - 1) {
						// create new computations
						//System.out.println("Start of " + v);
//						increaseCounter();
						GraphCompute gc = new GraphCompute(v, null, list, 1);
						gc.next = prev;
						gc.fork();
						prev = gc;
					}
				}

				// retrieve results from pending computations
				while (prev != null) {
					count += prev.join();
					prev = prev.next;
				}
			} 
			else {
				
				for (int k = 0; k < candidates.size(); k++) {
					
					int node = candidates.get(k);
					IntArray neighbors = graph.getNeighbors(node);
					
					// check if "node" has sufficient neighbors to complete a clique
					if (neighbors == null || neighbors.size() < cliqueSize - depth - 1) {
						continue;
					}
					//System.out.println("Neighborhood of " + node + " from " + this.vertex);
//					increaseCounter();

					// compute the intersection
					IntArray nextCandidates = IntArray.intersection(candidates, neighbors);
					
					// check if there are sufficient nodes to complete a clique in "nextCandidates"
					if (nextCandidates == null || nextCandidates.size() < cliqueSize - depth - 1)
						continue;

					// check if two nodes are missing to complete a clique ("node" + 1)
					if (depth == cliqueSize - 2) {
						for (int i = 0; i < nextCandidates.size(); i++){
							/*String found = "Found clique:";
							found += " " + nextCandidates.get(i);
							found += " " + node;
							for (GraphCompute w = this; w != null; w = w.parent) {
								found = found + " " + w.vertex;
							}
							
							System.out.println(found);*/
							count += 1;
						}
					} 
					else {
					    // create new computations
						GraphCompute gc = new GraphCompute(node, this, nextCandidates, depth + 1);
						gc.next = prev;
						gc.fork();
						prev = gc;
					}
				}

				// retrieve results from pending computations
				while (prev != null) {
					count += prev.join();
					prev = prev.next;
				}
			}
			return count;
		}

	}

	private void prepareForCounting() {

		OneByOne gi = new OneByOne();
		
		for (int u = 0; u < this.nodes.arrNodes.length; u++){
			IntArray uList = this.getNeighbors(u);
			
			if(uList == null)
				continue;
			
			for (int i = 0; i < uList.size(); i++) {
				
				int v = uList.get(i);
				IntArray vList = this.getNeighbors(v);
								
				if (uList.size() < vList.size() || (uList.size() == vList.size() && u < v))
					gi.addEdge(u, v);
			}
		}

		this.nodes = gi.nodes;
		
		/*int max = 0;
		double sum = 0;
		double number = 0;
		double larger = 0;
		double less = 0;
		DecimalFormat df = new DecimalFormat("#.##");*/
				
		// sort neighbors list
		for (int u = 0; u < this.nodes.arrNodes.length; u++){
			
			if(this.nodes.get(u) == null)
				continue;
			
			/*number++;
			
			sum += this.nodes.get(u).size();
			
			if (this.nodes.get(u).size() > max)
				max = this.nodes.get(u).size();*/

			this.nodes.get(u).sort();
		}
		
		/*System.out.println("Number of sets: " + df.format(number));
		System.out.println("Sum of candidates: " + df.format(sum));
		System.out.println("Average: " + df.format(sum/number));
		System.out.println("Max candidate size: " + max);
		
		int[] numberArr = new int[max+1];
		
		for (int u = 0; u < this.nodes.arrNodes.length; u++){
			if (nodes.get(u) == null)
				continue;
			
			numberArr[nodes.get(u).size()]++;
			if (nodes.get(u).size() > (sum/number))
				larger++;
			else
				less++;
		}
		
		System.out.println("Less then AVG: " + df.format((less*100)/number) + "%");
		System.out.println("Larger then AVG: " + df.format((larger*100)/number) + "%");
		
		
		System.out.println();
		for (int u = 1; u <= max; u++)
			System.out.println("Sets of size " + u + ": " + numberArr[u]);
		
		
		System.exit(0);*/
	}

}
