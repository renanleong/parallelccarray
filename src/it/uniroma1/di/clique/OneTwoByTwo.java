package it.uniroma1.di.clique;

import it.uniroma1.di.clique.util.IntArray;
import it.uniroma1.di.clique.util.IntToIntArray;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

public class OneTwoByTwo implements CliqueCounterGraph {

    protected IntToIntArray nodes = new IntToIntArray();
    public static OneTwoByTwo instance;
    public int cliqueSize;
//    public static volatile long counterNeighborhoods = 0;
    
    private OneTwoByTwo() {}

    public OneTwoByTwo(String pathFile) throws IOException {

        instance = this;
//        counterNeighborhoods = 0;

        // parse the input file and add edges to the graph
        try {

            BufferedReader br = new BufferedReader(new FileReader(pathFile));
            String line;
            while ((line = br.readLine()) != null) {

                String[] l = line.split(" ");
                if (l.length == 1)
                    l = line.split("\t");

                this.addEdge(l[0], l[1]);
            }
            br.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public long countCliquesOfSize(int cliqueSize, int parallelism) {

    	// compute the total order ≺ over the nodes and sort neighbors lists
        prepareForCounting();

        this.cliqueSize = cliqueSize;

        // create pool of threads
        ForkJoinPool fjPool = new ForkJoinPool(parallelism);

        // start computation
        return fjPool.invoke(new GraphCompute(-1, -1, null, null, 0));
    }

    @Override
    public void addEdge(String a, String b) {

        int v = Integer.parseInt(a);
        int y = Integer.parseInt(b);

        if (nodes.containsKey(v)) {
            nodes.get(v).add(y);
        } else {
            IntArray arr = new IntArray();
            arr.add(y);
            nodes.put(v, arr);
        }
    }

    public void addEdge(int v, int y) {

        if (nodes.containsKey(v)) {
            nodes.get(v).add(y);
        } else {
            IntArray arr = new IntArray();
            arr.add(y);
            nodes.put(v, arr);
        }
    }

    @Override
    public int getNodesNumber() {
        return nodes.size();
    }

    public IntArray getNeighbors(int node) {
        return nodes.get(node);
    }

//    public static synchronized void increaseCounter() {
//		counterNeighborhoods += 1;
//	}
    
    class GraphCompute extends RecursiveTask<Long> {

        private int first;
        private int second;
        public GraphCompute parent = null;
        public GraphCompute next = null;
        public IntArray candidates = null;
        private int depth;

        public GraphCompute(int first, int second, GraphCompute parent, IntArray candidates, int depth) {
            this.first = first;
            this.second = second;
            this.parent = parent;
            this.candidates = candidates;
            this.depth = depth;
        }

        @Override
        protected Long compute() {

            OneTwoByTwo graph = OneTwoByTwo.instance;
            int cliqueSize = graph.cliqueSize;
            GraphCompute prev = null;

            long count = 0;
            
            // Check if it is the first computation
            if (first < 0) {
            	
            	for(int u = 0; u < graph.getNodesNumber(); u++){ 
                	
                	// check if "u" has sufficient neighbors to complete a clique
                	if(nodes.get(u) == null || nodes.get(u).size() < cliqueSize - 1)
                		continue;
                	
//                	increaseCounter();
                	
                	IntArray nextCandidates = nodes.get(u);
                	
                	// create new computations
                	GraphCompute gc = new GraphCompute(u, -1, null, nextCandidates, 1);
                    gc.next = prev;
                    gc.fork();
                    prev = gc;
                }
                
                //retrieve results from pending computations
        		while (prev != null) {
            		count += prev.join();
                	prev = prev.next;
            	}
            } 
            else {

                for (int k = 0; k < candidates.size(); k++) {

                    int u = candidates.get(k);
                    
                    // check if "u" has sufficient neighbors to complete a clique
                    if(nodes.get(u) == null || nodes.get(u).size() < cliqueSize - (depth + 1))
                    	continue;
                    
//                    increaseCounter();
                    
                    // compute the intersection
                    IntArray firstIntersection = IntArray.intersection(candidates, nodes.get(u));
                	
                    // check if two nodes are missing to complete a clique ("u" + 1)
                    if(depth == cliqueSize - 2){
                    	
                		if(firstIntersection == null)
                			continue;
                    	
                    	count += firstIntersection.size();
               	
                    }
                    else{
                    	
                    	// check if there are sufficient nodes to complete a clique in "firstIntersection"
                    	if(firstIntersection == null || firstIntersection.size() < cliqueSize - (depth + 1))
                			continue;
                    	
                    	for(int i = 0; i < firstIntersection.size(); i++){
                    
                    		int v = firstIntersection.get(i);
                    		
                    		// check if "v" has sufficient neighbors to complete a clique
                    		if(nodes.get(v) == null || nodes.get(v).size() < cliqueSize - (depth + 2))
                        		continue;
                    		
//                    		increaseCounter();
                    		                    		
                    		// compute the intersection
                    		IntArray nextCandidates = IntArray.intersection(firstIntersection, nodes.get(v));
                    	
                    		// check if there are sufficient nodes to complete a clique in "nextCandidates"
                    		if(nextCandidates == null || nextCandidates.size() < cliqueSize - (depth + 2))
                            	continue;
                    		
                    		// check if three nodes are missing to complete a clique ("u" + "v" + 1)
                        	if(depth == cliqueSize - 3){
                            	count += nextCandidates.size();
                        	}
                        	else{
                        		// create new computations
                           		GraphCompute gc = new GraphCompute(u, v, this, nextCandidates, depth + 2);
                           		gc.next = prev;
                            	gc.fork();
                            	prev = gc;
                        	}
                    	}
                    }
                }

                //retrieve results from pending computations
                while (prev != null) {
                    count += prev.join();
                    prev = prev.next;
                }
            }
            return count;
        }

    }

    private void prepareForCounting() {

        OneTwoByTwo gi = new OneTwoByTwo();
        
        for (int u = 0; u < this.nodes.arrNodes.length; u++){
            IntArray uList = this.getNeighbors(u);
            
            if(uList == null)
				continue;
				
            for (int i = 0; i < uList.size(); i++) {

                int v = uList.get(i);
                IntArray vList = this.getNeighbors(v);

                if (uList.size() < vList.size() || (uList.size() == vList.size() && u < v))
                    gi.addEdge(u, v);
            }
        }

        this.nodes = gi.nodes;

        // sort neighbors list
        for (int u = 0; u < this.nodes.arrNodes.length; u++){
			
			if(this.nodes.get(u) == null)
				continue;
         
			this.nodes.get(u).sort();
        }
    }
}