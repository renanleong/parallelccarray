package it.uniroma1.di.clique;

public interface CliqueCounterGraph {
	long countCliquesOfSize(int cliqueSize, int parallelism);
	void addEdge (String a, String b);
	int getNodesNumber();
}
